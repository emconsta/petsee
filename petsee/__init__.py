__name__ = "petsee"
__package__ = "petsee"
__version__ = '0.0.1'
__author__ = "Emil Constantinescu"

from sys import exit
try:
    import guizero
except ImportError:
    print("guizero did not import successfully. Please check your setup.")
    exit(1)

from . import utilities as utils
from .utilities import _read_config