import numpy as np
from guizero import App, Text

if __name__ == "__main__":
    app = App("PETSc Command Line GUI")
    app.display()