import os
from setuptools import setup, find_packages
from guizero import __name__, __package__, __version__, __author__

## This is a Python 3 package only
from sys import version_info
if version_info.major != 3:
    print("This package will only work with Python 3. \n"
          "If you already have Python 3 installed try 'pip3 install guizero'.")

__desc__ = 'Python GUI interface for PETSc command line options'
__author_email__ = 'emconsta101@gmail.com'
__license__ = 'GPL'
__url__ = 'https://gitlab.com/emconsta/petsee'
__requires__ = []
__extras_require__={
        'images':  ["pillow>=4.3.0"]
    }
__python_requires__ = ">=3"
__keywords__ = [
    "GUI",
    "petsc",
    "petsee",
    "interface",
]
__classifiers__ = [
    "Development Status :: 1 - Initial Stage",
    "Topic :: Education",
    "Topic :: Software Development :: User Interfaces",
    "Topic :: Education",
    "Intended Audience :: Education",
    "Intended Audience :: Developers",
    "License :: OSI Approved :: GPL License",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.5",
    "Programming Language :: Python :: 3.6",
    "Programming Language :: Python :: 3.7",
    "Programming Language :: Python :: 3.8",
    "Programming Language :: Python :: 3.9",
    "Programming Language :: Python :: 3.10",
]
__long_description__ = """# petsee
petsee is designed to expose command line options in PETSc [www.petsc.org](https://www.petsc.org)
## Install

## Use
petsee is simple to use.
```python
python petsee.py
```
"""

setup(
    name=__name__,
    version=__version__,
    author=__author__,
    author_email=__author_email__,
    description=__desc__,
    long_description=__long_description__,
    long_description_content_type='text/markdown',
    license=__license__,
    keywords=__keywords__,
    url=__url__,
    packages=find_packages(),
    classifiers=__classifiers__,
    install_requires=__requires__,
    extras_require = __extras_require__,
    python_requires=__python_requires__,
)